package com.logicsoul.ls.tourrajastan.adapters;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.logicsoul.ls.tourrajastan.R;
import com.logicsoul.ls.tourrajastan.ui.activity.Activity_NavigationDrawer;
import com.logicsoul.ls.tourrajastan.ui.fragment.Fragment_Gallery;

/**
 * Created by lenovo on 9/26/2017.
 */

public class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView mActionName;
    public ImageView mActionImage;

    public HomeViewHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);
        mActionName = (TextView)itemView.findViewById(R.id.country_name);
        mActionImage = (ImageView)itemView.findViewById(R.id.country_photo);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Event Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
