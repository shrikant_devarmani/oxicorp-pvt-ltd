package com.logicsoul.ls.tourrajastan.ui.fragment;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.logicsoul.ls.tourrajastan.Model.EventHome;
import com.logicsoul.ls.tourrajastan.R;
import com.logicsoul.ls.tourrajastan.adapters.RV_HomeAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Fragment_TourHome extends Fragment implements ViewPagerEx.OnPageChangeListener, BaseSliderView.OnSliderClickListener {

    //for image sliding slider layout
    private SliderLayout mSlidingView;

    //hash map for images from local to load
    private HashMap<String, Integer> HashMapForLocalRes ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.activity_tour_rajastan_home,container,false);

        List<EventHome> rowListItem = getAllItemList();

        //getting initialising recycler view
        RecyclerView rView = (RecyclerView) view.findViewById(R.id.list_tour_rajastan_home);
        GridLayoutManager lLayout = new GridLayoutManager(getActivity(), 3);
        rView.setLayoutManager(lLayout);

        //For image slider
        mSlidingView = (SliderLayout) view.findViewById(R.id.image_slide);
        addImageToSlide();

        //adding names and setting click listener on slider
        for(String name : HashMapForLocalRes.keySet()){
            TextSliderView textSliderView = new TextSliderView(getContext());
            textSliderView  .description(name)
                            .image(HashMapForLocalRes.get(name))
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra",name);
            mSlidingView.addSlider(textSliderView);
        }
        mSlidingView.setPresetTransformer(SliderLayout.Transformer.DepthPage);
        mSlidingView.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSlidingView.setCustomAnimation(new DescriptionAnimation());
        mSlidingView.setDuration(3000);
        mSlidingView.addOnPageChangeListener(this);


        //Adapter object creation
        RV_HomeAdapter rcAdapter = new RV_HomeAdapter(getActivity(), rowListItem);
        rView.setAdapter(rcAdapter);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStop() {
        //stop the sliding when in background
        mSlidingView.startAutoCycle();
        super.onStop();
    }

    @Override
    public void onPause() {
        //stop the sliding when in background
        mSlidingView.startAutoCycle();
        super.onPause();
    }

    //Add image from local
    public void addImageToSlide(){
        //init for local resources for sliding
        HashMapForLocalRes = new HashMap<>();
        HashMapForLocalRes.put("Adventure", R.drawable.advantures);
        HashMapForLocalRes.put("Destination", R.drawable.destination);
        HashMapForLocalRes.put("Fair Festival", R.drawable.fair_festival);
        HashMapForLocalRes.put("Forts", R.drawable.forts);
        HashMapForLocalRes.put("Wild Life", R.drawable.wildlife1);
        HashMapForLocalRes.put("Lakes", R.drawable.lakes);
        HashMapForLocalRes.put("Places to Visit", R.drawable.places_to_visit);
        HashMapForLocalRes.put("Museums", R.drawable.mesuems1);
    }

    //Function for events of Tour Rajastan
    private List<EventHome> getAllItemList(){
        List<EventHome> allItems = new ArrayList<>();

        allItems.add(new EventHome("Advanture", R.drawable.advantures));
        allItems.add(new EventHome("Destination", R.drawable.destination));
        allItems.add(new EventHome("Fair-Festival", R.drawable.fair_festival));
        allItems.add(new EventHome("Forts", R.drawable.forts));
        allItems.add(new EventHome("Wildlife", R.drawable.wildlife1));
        allItems.add(new EventHome("Lakes", R.drawable.lakes));
        allItems.add(new EventHome("Places to visit", R.drawable.places_to_visit));
        allItems.add(new EventHome("Meseums", R.drawable.mesuems1));
        allItems.add(new EventHome("Other", R.drawable.mesuems));

        return allItems;
    }

    //Method first for Slider layout library
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }
    //Method second for Slider layout library
    @Override
    public void onPageSelected(int position) {

    }
    //Method third for Slider layout library
    @Override
    public void onPageScrollStateChanged(int state) {

    }

    //This is for text slider view of on text click listener
    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
