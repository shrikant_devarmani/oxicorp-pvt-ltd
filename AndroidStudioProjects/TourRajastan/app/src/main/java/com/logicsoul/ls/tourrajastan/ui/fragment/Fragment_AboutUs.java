package com.logicsoul.ls.tourrajastan.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.logicsoul.ls.tourrajastan.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_AboutUs extends Fragment {

    private TextView mTxtAboutUs;

    public Fragment_AboutUs() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__about_us, container, false);
        mTxtAboutUs = (TextView) view.findViewById(R.id.text_about_us);

        //This is because i'm adding fragment and on click of this fragment previous fragment content should not load
        // so handle click event here
        mTxtAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do Nothing
            }
        });
        return view;
    }

}
