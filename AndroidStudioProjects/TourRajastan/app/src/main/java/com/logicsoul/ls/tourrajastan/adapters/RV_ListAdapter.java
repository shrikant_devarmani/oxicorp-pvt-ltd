package com.logicsoul.ls.tourrajastan.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicsoul.ls.tourrajastan.Model.EventHome;
import com.logicsoul.ls.tourrajastan.R;

import java.util.List;

/**
 * Created by lenovo on 9/28/2017.
 */

public class RV_ListAdapter extends RecyclerView.Adapter<ListViewHolder> {

    private List<EventHome> mItemList;
    private Context mContext;
    private LayoutInflater mInflater;

    public RV_ListAdapter(Context context, List<EventHome> itemList){
        mContext = context;
        mItemList = itemList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = mInflater.from(parent.getContext()).inflate(R.layout.fragment_action_select, null);
        ListViewHolder rcv = new ListViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        holder.mEventName.setText(mItemList.get(position).getEventName());
        holder.mEventDescription.setText(mItemList.get(position).getEventDescription());
        holder.mEventImage.setImageResource(mItemList.get(position).getEventImage());
    }

    @Override
    public int getItemCount() {
        return this.mItemList.size();
    }
}
