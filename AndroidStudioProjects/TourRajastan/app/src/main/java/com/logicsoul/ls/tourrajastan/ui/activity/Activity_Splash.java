package com.logicsoul.ls.tourrajastan.ui.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.logicsoul.ls.tourrajastan.R;

public class Activity_Splash extends AppCompatActivity {

        private int SPLASH_TIMEOUT = 1000;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_activit__splash);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent intent = new Intent();
                        intent.setClass(Activity_Splash.this,Activity_NavigationDrawer.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            },SPLASH_TIMEOUT);
        }

}
