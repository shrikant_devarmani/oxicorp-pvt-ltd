package com.logicsoul.ls.tourrajastan.ui.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.logicsoul.ls.tourrajastan.R;
import com.logicsoul.ls.tourrajastan.ui.fragment.Fragment_AboutUs;
import com.logicsoul.ls.tourrajastan.ui.fragment.Fragment_TourHome;


public class Activity_NavigationDrawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //for same fragment instance not going to add into back stack
    private Fragment fragmentPrev = null;
    private boolean firstTime = true ;

    private DrawerLayout drawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private View.OnClickListener originalToolbarListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
//Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR
// and set windowActionBar to false in your theme to use a Toolbar instead.
        if(getSupportActionBar() == null) {
            setSupportActionBar(toolbar);
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        originalToolbarListener = toggle.getToolbarNavigationClickListener();
        getSupportFragmentManager().addOnBackStackChangedListener(new BackStackChangeListener());

        // for first time home screen fragment
        displaySelectedScreen(R.id.nav_home);
    }

    private void displaySelectedScreen(int itemId) {
        //creating fragment object
        Fragment fragment = null;
        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_home:
                fragment = new Fragment_TourHome();
                fragmentPrev = fragment ;
                break;

            /*case R.id.nav_work_offline:
                //handle only the on of for the online and offline data and store the details in shared preference and according to act
                break;*/

            case R.id.action_about_us:
                //About us fragment
                Fragment_AboutUs aboutUs = new Fragment_AboutUs();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.content_frame,aboutUs,"AboutUs");
                fragmentTransaction.addToBackStack("AboutUs");
                fragmentTransaction.commitAllowingStateLoss();
                break;

           /* case R.id.nav_send:
                //send the link of app on play store to friend / other
                break;*/

            case R.id.nav_share:
                //sharing the application
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                //setting the content type of supportive application
                sharingIntent.setType("text/plain");
                //TODO : replace the <p> </p> with play store application link
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>This Rajastan tourism application play store link is comping soon.</p>"));
                startActivity(Intent.createChooser(sharingIntent,"Share using"));
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            if(firstTime) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.addToBackStack(fragment.getClass().getName());
                ft.replace(R.id.content_frame, fragment);
                ft.commit();
                //manage is first time then only backstack
                firstTime = false;
            }else{
                //checking weather clicked fragment and previous fragment is same then skip else add fragment
                if(fragment.getClass().equals(fragmentPrev.getClass())){
                    //Toast.makeText(this, "Same fragment"+fragment.getClass().getName() + " ***  " +fragmentPrev.getClass().getName(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, "You are already AT HOME", Toast.LENGTH_SHORT).show();
                }else {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.addToBackStack(fragment.getClass().getName());
                    ft.replace(R.id.content_frame, fragment);
                    ft.commit();
                }
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
            if (backStackEntryCount == 1) {
                finish();
            } else {
                if (backStackEntryCount > 1) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {

            case R.id.action_about_us:
                //About us fragment
                Fragment_AboutUs aboutUs = new Fragment_AboutUs();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.content_frame,aboutUs,"AboutUs");
                fragmentTransaction.addToBackStack("AboutUs");
                fragmentTransaction.commitAllowingStateLoss();
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        displaySelectedScreen(id);
        return true;
    }

    //Inner class for back stack change listener
    private class BackStackChangeListener implements FragmentManager.OnBackStackChangedListener {
        @Override
        public void onBackStackChanged() {
            final DrawerArrowDrawable drawerArrow = new DrawerArrowDrawable(Activity_NavigationDrawer.this);
            drawerArrow.setColor(Color.RED);

            int i = getSupportFragmentManager().getBackStackEntryCount();

            if (getSupportFragmentManager() != null) {
                if (i > 1) {
                    toggle.setDrawerIndicatorEnabled(false);
                    toolbar.setNavigationIcon(drawerArrow);
                    ObjectAnimator.ofFloat(drawerArrow, "progress", 1).start();
                    toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSupportFragmentManager().popBackStack();
                            ObjectAnimator.ofFloat(drawerArrow, "progress", 0).start();
                        }
                    });
                } else {
                    toggle.setDrawerIndicatorEnabled(true);
                    toggle.setToolbarNavigationClickListener(originalToolbarListener);
                }
            }
        }
    }
}

