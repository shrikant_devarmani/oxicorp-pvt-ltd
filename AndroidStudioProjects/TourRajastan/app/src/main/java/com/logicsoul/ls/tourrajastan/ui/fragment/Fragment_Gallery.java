package com.logicsoul.ls.tourrajastan.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicsoul.ls.tourrajastan.Model.EventHome;
import com.logicsoul.ls.tourrajastan.R;
import com.logicsoul.ls.tourrajastan.adapters.RV_ListAdapter;

import java.util.ArrayList;
import java.util.List;

public class Fragment_Gallery extends Fragment {

    private List<EventHome> mRowListItem ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__gallery, container, false);

        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.action_selected);
        LinearLayoutManager mManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mManager);

        mRowListItem  = getAllItemList();
        RV_ListAdapter mAdapter = new RV_ListAdapter(getActivity() ,mRowListItem);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        return view;
    }

    //Function for events of Tour Rajastan
    //TODO : response structure is same may manage according to position clicked or o worry if response is same OR send key to request like forts or advanture to request to get releated data
    //TODO : for no places array pass the city co - ordinates instead of empty
    private List<EventHome> getAllItemList(){
        List<EventHome> allItems = new ArrayList<>();

        allItems.add(new EventHome("Hot Air Balloon Rides", R.drawable.hot_air_baloon_ride,"When it comes to exploring the beautiful landscape of Rajasthan, a hot air balloon ride is one of the best options. Soar above the vibrant Pushkar festival and treat yourself to breath-taking views. You can also enjoy the beauty of India’s ‘Pink City’, and absorb its colours, flavours and sounds as you take in magnificent forts, palaces and bewitching architecture that Jaipur is known for. Hot air balloon rides are available across the subject to prior booking"));
        allItems.add(new EventHome("Cycling", R.drawable.cycling,"Cycling is the most fun and cost-effective way to take in the rich landscape of Rajasthan. Bicycles can be hired almost anywhere, and are sturdy and unlikely to develop snags while riding. However, if you’re looking forward to doing some serious cycling in the state, opt for the inexpensive, geared bicycles that are easily available in all major towns. Bicycles are ideal for exploring villages that are close to big towns such as Udaipur, Kumbhalgarh and Ranakpur, or for getting around the lush hills of places like Mount Abu"));
        allItems.add(new EventHome("Camping", R.drawable.camping,"Camping under the sparkling skies of Rajasthan allows you to feast your eyes on the unique view of celestial wonders at night. Camping is some sort of an art form here, with established campsites being set in several locations that would befit royalty. Camping expeditions in Rajasthan beautifully blend adventure with comfort and offer a truly one-of-a-kind outdoor experience. Personalised tents equipped with a bedroom, cosy hallway or leisure area and an attached bath are also available for rent. Some tents even come with hand-block printed fabrics on their interiors. For the adventurous folk, the ideal time to head out for camping would be between October and March. One can set up tents at the welcoming Damodra Village, get pampered at The Serai in Jaisalmer or enjoy the contemporary camp sites at Sam village."));
        allItems.add(new EventHome("Paragliding", R.drawable.paragliding_1,"Still in its initial stage, paragliding is a sport that must feature on your itinerary, regardless of whether you’re an adventure enthusiast or casual vacationer. Create unforgettable memories as you soar above the beautiful, crimson-sand countryside of Rajasthan. This activity is, so far, only offered by operators who have the requisite equipment with them."));
        allItems.add(new EventHome("Water Sports", R.drawable.water_sports,"One does not think of Rajasthan when it comes to water sports, but this desert paradise has something to offer on that front too. Rajasthan has no dearth of vast, blue lakes and water bodies where one can enjoy boating and other water sports.."));
        allItems.add(new EventHome("Desert and Camel Safari", R.drawable.desert_and_camel_safari1,"Life in the villages around Jaisalmer, Bikaner, Jodhpur (Osian), Pushkar and Nagaur pulses to the sound of music and sways in a profusion of colours, where men with proud moustaches in elegant turbans, cheerful women in vivid ethnic attire and children, live life to the hilt. Summers here end in dancing, singing and festivities. However, that’s not all this enigmatic land has to offer. Thar Desert and its Camel Safari –a treat to tourists since 1998 – offers visitors a fun, vibrant and memorable experience. The Camel Safari gives you a glimpse into the rustic lifestyle of the desert villages and its people. Traverse through sand dunes, past ancient havelis, temples and even historic landmarks. What’s more? Treat your palate to delicious, authentic cuisine and traditional music at various stops on the safari tour.."));
        allItems.add(new EventHome("Wild Life Safari", R.drawable.wildlife_safari,"Life in the villages around Jaisalmer, Bikaner, Jodhpur (Osian), Pushkar and Nagaur pulses to the sound of music and sways in a profusion of colours, where men with proud moustaches in elegant turbans, cheerful women in vivid ethnic attire and children, live life to the hilt. Summers here end in dancing, singing and festivities. However, that’s not all this enigmatic land has to offer. Thar Desert and its Camel Safari –a treat to tourists since 1998 – offers visitors a fun, vibrant and memorable experience. The Camel Safari gives you a glimpse into the rustic lifestyle of the desert villages and its people. Traverse through sand dunes, past ancient havelis, temples and even historic landmarks. What’s more? Treat your palate to delicious, authentic cuisine and traditional music at various stops on the safari tour."));
        return allItems;
    }
}


