package com.logicsoul.ls.tourrajastan.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicsoul.ls.tourrajastan.Model.EventHome;
import com.logicsoul.ls.tourrajastan.R;
import com.logicsoul.ls.tourrajastan.ui.activity.Activity_NavigationDrawer;
import com.logicsoul.ls.tourrajastan.ui.fragment.Fragment_Gallery;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lenovo on 9/26/2017.
 */

public class RV_HomeAdapter extends RecyclerView.Adapter<HomeViewHolder> {

    private List<EventHome> itemList;
    private Context mContext;

    public RV_HomeAdapter(Context context, List<EventHome> itemList) {
        this.itemList = itemList;
        this.mContext = context;
    }

    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(mContext).inflate(R.layout.card_view_list, null);
        HomeViewHolder rcv = new HomeViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, final int position) {

        holder.mActionName.setText(itemList.get(position).getEventName());
        holder.mActionImage.setImageResource(itemList.get(position).getEventImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity_NavigationDrawer activity_navigationDrawer= (Activity_NavigationDrawer) v.getContext();

                Bundle bundle = new Bundle();
                bundle.putSerializable("data", (Serializable) itemList);

                //create fragment instance
                Fragment_Gallery fragment = new Fragment_Gallery();
                //assign data to fragment
                fragment.setArguments(bundle);
                //start activity and add to backstack
                FragmentTransaction ft =  activity_navigationDrawer.getSupportFragmentManager().beginTransaction();
                ft.addToBackStack("Item"+itemList.get(position).getEventName());
                ft.add(R.id.content_frame, fragment);
                ft.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}