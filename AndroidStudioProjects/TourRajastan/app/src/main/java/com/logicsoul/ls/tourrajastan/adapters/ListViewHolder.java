package com.logicsoul.ls.tourrajastan.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.logicsoul.ls.tourrajastan.R;

/**
 * Created by lenovo on 9/28/2017.
 */

public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView mEventName;
    public TextView mEventDescription;
    public ImageView mEventImage;

    public ListViewHolder(View view) {
        super(view);

        view.setOnClickListener(this);
        mEventName = (TextView)view.findViewById(R.id.content_name);
        mEventDescription = (TextView)view.findViewById(R.id.content_description);
        mEventImage = (ImageView) view.findViewById(R.id.image_view);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(v.getContext(), "Clicked Event Position = " + getPosition(), Toast.LENGTH_SHORT).show();
        //TODO : handling the click event of lis action item
    }
}
