package com.logicsoul.ls.tourrajastan.Model;

import java.io.Serializable;

/**
 * Created by lenovo on 9/26/2017.
 */

public class EventHome implements Serializable {
    private int mEventImage;
    private String mEventName;
    private String mEventDescription;

    public EventHome( String mEventName ,int mEventImage) {
        this.mEventImage = mEventImage;
        this.mEventName = mEventName;
    }

    public EventHome( String mEventName, int mEventImage, String mEventDescription) {
        this.mEventImage = mEventImage;
        this.mEventName = mEventName;
        this.mEventDescription = mEventDescription;
    }

    public String getEventDescription() {
        return mEventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.mEventDescription = eventDescription;
    }

    public int getEventImage() {
        return mEventImage;
    }

    public void setEventImage(int eventImage) {
        this.mEventImage = eventImage;
    }

    public String getEventName() {
        return mEventName;
    }

    public void setEventName(String eventName) {
        this.mEventName = eventName;
    }
}
