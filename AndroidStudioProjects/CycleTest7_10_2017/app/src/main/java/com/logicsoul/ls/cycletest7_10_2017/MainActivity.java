package com.logicsoul.ls.cycletest7_10_2017;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    private StringRequest mStringRequest;
    private RequestQueue mRequestQueue;
    private String url = "http://www.bitcodeindia.com/android/getAllPeople.php?cmd=get";
    private ArrayList<People> peopleList = new ArrayList<>();
    private ListView mListView;
    private AdapterClass mAdapter;
    private Button mBtnNext;
    private EventBus myEventBus;
    private ArrayList<People> pe_send_list;
    private  DBHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myEventBus = EventBus.getDefault();
        myEventBus.getDefault().register(this);
        pe_send_list = new ArrayList<>();
        helper = new DBHelper(MainActivity.this);

        mRequestQueue = Volley.newRequestQueue(this);
        mListView = (ListView) findViewById(R.id.list_item);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());

                    String status = jsonObject.getString("status");

                    JSONArray jsonArray = jsonObject.getJSONArray("result");

                    for(int i= 0 ; i<jsonArray.length();i++){

                        JSONObject object = (JSONObject) jsonArray.get(i);

                        String person_id = object.getString("person_id");
                        String person_name= object.getString("person_name");
                        String person_image = object.getString("person_image");
                        String person_lat = object.getString("person_lat");
                        String person_long = object.getString("person_long");
                        String person_email = object.getString("person_email");
                        String person_contact = object.getString("person_contact");
                        String person_job_title = object.getString("person_job_title");


                        People people = new People(person_id,person_name,person_image,person_lat,person_long,person_email,person_contact,person_job_title);
                        peopleList.add(people);

                    }

                    mAdapter = new AdapterClass(MainActivity.this,peopleList);
                    mListView.setAdapter(mAdapter);


                    long inse = helper.addPeople(peopleList);
                    if(inse > 0){
                        Toast.makeText(MainActivity.this, "Record inserted", Toast.LENGTH_SHORT).show();
                    }

                    mAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Error" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });



        if(isNetworkAvailable()) {
            mRequestQueue.add(mStringRequest);
        }else {
                ArrayList<People > peopleGot = helper.getPeople();
                mAdapter = new AdapterClass(MainActivity.this,peopleGot);
                mListView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
        }


        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,DetailActivity.class);
                intent.putExtra("data",pe_send_list);
                startActivity(intent);
            }
        });
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void onEvent(ArrayList<People> peopleList){
        Toast.makeText(this, "Toast"+peopleList.toString(), Toast.LENGTH_SHORT).show();
        pe_send_list = peopleList;
    }
}
